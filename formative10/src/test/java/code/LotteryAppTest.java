package code;

import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

public class LotteryAppTest {

    @Test
    public void testLotteryApp() {
        int numberParticipants = 0;
        Set<Participant> participantsSet = new HashSet<>();
        LotteryApp.initializeParticipants(numberParticipants, participantsSet);

        while (!participantsSet.isEmpty()) {
            LotteryApp.runLottery(participantsSet);
        }
        assertTrue(participantsSet.isEmpty());
    }


    @Test
    public void testInitializeParticipants() {
        int numberParticipants = 10;
        Set<Participant> participantsSet = new HashSet<>();
        LotteryApp.initializeParticipants(numberParticipants, participantsSet);

        assertEquals(numberParticipants, participantsSet.size());
    }

    @Test
    public void testTakeRandomCode() {
        int numberParticipants = 5;
        Set<Participant> participantsSet = new HashSet<>();
        LotteryApp.initializeParticipants(numberParticipants, participantsSet);

        LotteryApp.takeRandomCode(participantsSet);

        Set<String> uniqueCodes = new HashSet<>();
        for (Participant participant : participantsSet) {
            assertNotNull(participant.getRandomCode());
            assertTrue(uniqueCodes.add(participant.getRandomCode()));
        }
    }
}
