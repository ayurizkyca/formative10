package code;


import java.util.*;

public class LotteryApp {
    public static void main(String[] args) {
        int numberParticipant = 15;
        Set<Participant> participantsSet = new HashSet<>();

        initializeParticipants(numberParticipant, participantsSet);

        // Pengundian
        int meetingDay = 1;
        while (!participantsSet.isEmpty()) {
            System.out.println("Meeting Day-" + meetingDay);
            takeRandomCode(participantsSet);

            List<Participant> sortedParticipantsList = new ArrayList<>(participantsSet);
            Collections.sort(sortedParticipantsList);

            for (Participant participant : sortedParticipantsList) {
                System.out.println(participant.getName() + " - Code: " + participant.getRandomCode());
            }

            Participant theWinner = sortedParticipantsList.get(sortedParticipantsList.size() - 1);
            System.out.println("Winner in the-" + meetingDay + ": " + theWinner.getName());
            System.out.println("=============================================");

            participantsSet.remove(theWinner);
            meetingDay++;
        }
    }

    // Peserta mengambil random code untuk diundi
    private static void takeRandomCode(Set<Participant> participants) {
        for (Participant participant : participants) {
            String randomCode = generateRandomCode();
            participant.setRandomCode(randomCode);
        }
    }

    // Membuat random code
    private static String generateRandomCode() {
        String charCode = "ABCDEFGHIJKLMNOPQRSTUVW012345678910";
        StringBuilder randomCode = new StringBuilder();

        Random random = new Random();
        for (int i = 0; i < 5; i++) {
            char randomCharCode = charCode.charAt(random.nextInt(charCode.length()));
            randomCode.append(randomCharCode);
        }
        return randomCode.toString();
    }

    public static void initializeParticipants(int numberParticipant, Set<Participant> participantsSet) {
        for (int i = 1; i <= numberParticipant; i++) {
            Participant participant = new Participant("Participant-" + i, 150000);
            participantsSet.add(participant);
        }
    }

    public static Participant determineWinner(Set<Participant> participantsSet) {
        List<Participant> sortedParticipantsList = new ArrayList<>(participantsSet);
        Collections.sort(sortedParticipantsList);
        // Menentukan pemenang
        return sortedParticipantsList.get(sortedParticipantsList.size() - 1);
    }
}

