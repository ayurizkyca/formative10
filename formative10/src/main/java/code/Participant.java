package code;

public class Participant implements Comparable<Participant> {
    private String name;
    private int saldo;
    private String randomCode;

    public Participant(String name, int saldo, String randomCode) {
        this.name = name;
        this.saldo = saldo;
        this.randomCode = randomCode;
    }

    public Participant(String name, int saldo) {
        this.name = name;
        this.saldo = saldo;
    }

    public String getRandomCode() {
        return randomCode;
    }

    public void setRandomCode(String randomCode) {
        this.randomCode = randomCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSaldo() {
        return saldo;
    }

    public void setSaldo(int saldo) {
        this.saldo = saldo;
    }

    @Override
    public String toString() {
        return "{" +
                " name='" + name + "'" +
                ", saldo='" + saldo + "'" +
                ", randomCode='" + randomCode + "'" +
                "}";
    }

    @Override
    public int compareTo(Participant participant) {
        return this.randomCode.compareTo(participant.randomCode);
    }
}
